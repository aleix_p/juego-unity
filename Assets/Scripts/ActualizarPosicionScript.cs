using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActualizarPosicionScript : MonoBehaviour
{
    public float velocidad = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Transform miTransform = GetComponent<Transform>();

        // OPCION 1: Movemos la esfera 0.25 en X y 0.25 en Y
        // miTransform.position += new Vector3(0.25f,0.25f,0);

        // OPCION 2: Mover en X en funci�n de la velocidad
        // miTransform.position += new Vector3(1 * velocidad, 0, 0);

        // OPCION 3: Las otras son igual de validas pero esta ser�a la opci�n correcta, darle movimiento continuo (AKA traslaci�n) en eje X
        miTransform.Translate(Vector3.forward * velocidad);

        // Tambi�n podemos rotarla:
        miTransform.Rotate(Vector3.up * velocidad);


    }
}
