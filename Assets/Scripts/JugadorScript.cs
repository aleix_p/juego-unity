using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorScript : MonoBehaviour
{
    public GameObject enemigoGO = null;
    public EnemigoScript enemigoScript = null;

    // Start is called before the first frame update
    void Start()
    {
        enemigoGO = GameObject.Find("CUBO-ENEMIGO");
        enemigoScript = enemigoGO.GetComponent<EnemigoScript>();
    }

    // Update is called once per frame
    void Update()
    {
        bool enemigoActivo = enemigoScript.enemigoActivo;

        if (enemigoActivo == false)
        {
            enemigoGO.SetActive(false);
        }
        else
        {
            enemigoGO.SetActive(true);
        }

        //  ALTERNATIVA:
        //  enemigoGO.SetActive(enemigoActivo);
    }
}
