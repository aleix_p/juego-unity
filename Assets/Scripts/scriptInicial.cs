using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptInicial : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // RECUPERAMOS EL NOMBRE DEL PROPIO GO
        Debug.Log("START: GameObject: " + gameObject.name);

        // RECUPERAMOS EL CUBO CON FIND
        GameObject cuboGO = null;
        cuboGO = GameObject.Find("CUBO");

        //MIRAMOS SU NOMBRE Y TAG
        if (cuboGO != null)
        {
            Debug.Log("HE ENCONTRADO EL GameObject: " + cuboGO.name);
            Debug.Log("EL TAG del GameObject: " + cuboGO.tag);
            Debug.Log("NOMBRE: " + cuboGO.name + " - TAG: " + cuboGO.tag);
        }

        // BUSCAR OBJETOS POR TAG
        GameObject[] arrayCubitosGO = null;
        arrayCubitosGO = GameObject.FindGameObjectsWithTag("cubito");

        // ITERAMOS EL ARRAY
        foreach (GameObject cubito in arrayCubitosGO)
        {
            Debug.Log("cubito --> " + cubito.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnEnable()
    {
    }
    private void OnDisable()
    {
    }

    private void OnMouseDown()
    {
        Debug.Log("La ESFERA ha sido clickada");
    }

    private void OnMouseEnter()
    {
        Debug.Log("La ESFERA ha sido se�alada");
    }

}
