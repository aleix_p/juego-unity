using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformacionesScript : MonoBehaviour
{
    public GameObject    cuboEnemigo   = null;

    // Start is called before the first frame update
    void Start()
    {
        Transform miTransform = GetComponent<Transform>();
        miTransform.position = new Vector3(3,3,3);

        cuboEnemigo = GameObject.Find("CUBO-ENEMIGO");

        Transform enemigoTransform = cuboEnemigo.GetComponent<Transform>();
        enemigoTransform.position = new Vector3(4,4,4);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
